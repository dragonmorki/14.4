﻿#include <iostream>
#include <string>

int main()
{
    std::string myString = "Hello, World!";

    std::cout << "String: " << myString << std::endl;
    std::cout << "Length: " << myString.length() << std::endl;
    std::cout << "First character: " << myString.front() << std::endl;
    std::cout << "Last character: " << myString.back() << std::endl;

    return 0;
}
